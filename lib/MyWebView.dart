import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class MyWebView extends StatefulWidget {
  MyWebView(
      {Key? key,
      required this.url,
      required this.isTokenGranted,
      required this.appBarText})
      : super(key: key);

  String url;
  String appBarText;
  Function isTokenGranted;

  @override
  State<MyWebView> createState() => _MyWebViewState();
}

class _MyWebViewState extends State<MyWebView> {
  @override
  void initState() {
    widget.isTokenGranted;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF364AFB),
        title: Text(widget.appBarText),
      ),
      body: WebView(
        debuggingEnabled: true,
        initialUrl: Uri.encodeFull(widget.url),
        javascriptMode: JavascriptMode.unrestricted,
        navigationDelegate: (request) {
          if (request.url.contains('auth?code')) {
            var uri = Uri.dataFromString(request.url);
            print(request.url);
            print(uri.queryParameters["code"]);
            widget.isTokenGranted();
            Navigator.pop(context); // Close current window
            return NavigationDecision.prevent; // Prevent opening url
          } else {
            return NavigationDecision.navigate; // Default decision
          }
        },
      ),
    );
  }
}
