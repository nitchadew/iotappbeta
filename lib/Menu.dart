// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:iotapp/inherited/StyleInherited.dart';

class MenuList {
  String imagePath;
  String url;

  MenuList({required this.imagePath, required this.url});
}

class Menu extends StatefulWidget {
  String serviceName;
  Menu({Key? key, required this.serviceName}) : super(key: key);

  @override
  State<Menu> createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  List menuList = [];

  @override
  void initState() {
    if (widget.serviceName == "visie") {
      menuList.add(MenuList(
          imagePath: 'images/menu_visie_Device.png',
          url: 'https://www.google.com/'));
      menuList.add(MenuList(
          imagePath: 'images/menu_visie_Location.png',
          url: 'https://www.google.com/'));
      menuList.add(MenuList(
          imagePath: 'images/menu_visie_Dashboard.png',
          url: 'https://www.google.com/'));
      menuList.add(MenuList(
          imagePath: 'images/menu_visie_Information.png',
          url: 'https://www.google.com/'));
      menuList.add(MenuList(
          imagePath: 'images/menu_visie_Request_data.png',
          url: 'https://www.google.com/'));
      menuList.add(MenuList(
          imagePath: 'images/menu_visie_Help.png',
          url: 'https://www.google.com/'));
    } else {
      menuList.add(MenuList(
          imagePath: 'images/menu_visie_Device.png',
          url: 'https://www.google.com/'));
      menuList.add(MenuList(
          imagePath: 'images/menu_visie_Location.png',
          url: 'https://www.google.com/'));
      menuList.add(MenuList(
          imagePath: 'images/menu_visie_Dashboard.png',
          url: 'https://www.google.com/'));
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Stack(alignment: Alignment.center, children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(65.0),
                bottomRight: Radius.circular(65.0)),
            color: Color(0xff5960F9),
            boxShadow: [
              BoxShadow(
                color: Color(0xff87F0FF),
                spreadRadius: 1,
                blurRadius: 0,
                offset: Offset(1, 2),
              ),
            ],
          ),
          height: MediaQuery.of(context).size.height * 0.116,
        ),
        Positioned(
          left: 15,
          top: MediaQuery.of(context).size.height * 0.045,
          child: ClipOval(
            child: Material(
              color: Colors.transparent, // Button color
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: SizedBox(
                    width: 56,
                    height: 56,
                    child: Icon(
                      Icons.arrow_back,
                      size: 27,
                      color: Colors.white,
                    )),
              ),
            ),
          ),
        ),
        Positioned(
          top: MediaQuery.of(context).size.height * 0.045,
          child: widget.serviceName == "visie"
              ? SizedBox(
                  height: 56,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("VISIE HEALTH TRACKING",
                        style: StyleInherited.of(context)!
                            .myTextStyle
                            .pageTitleStyle()),
                  ),
                )
              : SizedBox(
                  height: 56,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("FARM KIDS",
                        style: StyleInherited.of(context)!
                            .myTextStyle
                            .pageTitleStyle()),
                  ),
                ),
        )
      ]),
      Padding(
        padding: const EdgeInsets.only(left: 40.0, top: 35.0),
        child: Text("Menu",
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700)),
      ),
      Expanded(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: GridView.count(
            padding: const EdgeInsets.only(top: 25),
            mainAxisSpacing: 20,
            crossAxisSpacing: 20,
            crossAxisCount: 2,
            children: List.generate(menuList.length, (index) {
              return Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child:
                      Image.asset(menuList[index].imagePath, fit: BoxFit.fill));
            }),
          ),
        ),
      )
    ]);
  }
}
