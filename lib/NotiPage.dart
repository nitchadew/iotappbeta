// ignore_for_file: unnecessary_const

import 'package:flutter/material.dart';
import 'package:iotapp/inherited/StyleInherited.dart';

class NotiPage extends StatefulWidget {
  NotiPage({Key? key}) : super(key: key);

  @override
  State<NotiPage> createState() => _NotiPageState();
}

class _NotiPageState extends State<NotiPage> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(65.0),
                      bottomRight: Radius.circular(65.0)),
                  color: Color(0xff5960F9),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff87F0FF),
                      spreadRadius: 1,
                      blurRadius: 0,
                      offset: Offset(1, 2),
                    ),
                  ],
                ),
                height: MediaQuery.of(context).size.height * 0.116,
              ),
              Positioned(
                top: MediaQuery.of(context).size.height * 0.045,
                child: SizedBox(
                  height: 56,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("NOTIFICATION",
                        style: StyleInherited.of(context)!
                            .myTextStyle
                            .pageTitleStyle()),
                  ),
                ),
              )
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: [
                    // vise card
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20, bottom: 5, left: 5),
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width,
                        height: 75,
                        child: Row(
                          children: [
                            SizedBox(
                              height: 100,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(45),
                                child: Image.asset(
                                  'images/Homepage_logo_vise.png',
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 5),
                                    child: Text(
                                      "VISIE 1",
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Color(0xFF364AFC),
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                  Text(
                                    "OAI Floor 4",
                                    style: TextStyle(
                                        fontSize: 13.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Divider(
                      height: 0,
                      thickness: 1.5,
                      indent: 10,
                      endIndent: 10,
                      color: Color.fromARGB(175, 213, 213, 213),
                    ),
                    // vise card
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 5, bottom: 5, left: 5),
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width,
                        height: 75,
                        child: Row(
                          children: [
                            SizedBox(
                              height: 100,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(45),
                                child: Image.asset(
                                  'images/Homepage_logo_vise.png',
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 5),
                                    child: Text(
                                      "VISIE 2",
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Color(0xFF364AFC),
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                  Text(
                                    "OAI Floor 1",
                                    style: TextStyle(
                                        fontSize: 13.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Divider(
                      height: 0,
                      thickness: 1.5,
                      indent: 10,
                      endIndent: 10,
                      color: Color.fromARGB(175, 213, 213, 213),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 5, bottom: 5, left: 5),
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width,
                        height: 75,
                        child: Row(
                          children: [
                            SizedBox(
                              height: 100,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(45),
                                child: Image.asset(
                                  'images/Homepage_logo_vise.png',
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 5),
                                    child: Text(
                                      "VISIE 3",
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Color(0xFF364AFC),
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                  Text(
                                    "OAI Floor xxx",
                                    style: TextStyle(
                                        fontSize: 13.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Divider(
                      height: 0,
                      thickness: 1.5,
                      indent: 10,
                      endIndent: 10,
                      color: Color.fromARGB(175, 213, 213, 213),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 5, bottom: 5, left: 5),
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width,
                        height: 75,
                        child: Row(
                          children: [
                            SizedBox(
                              height: 100,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(45),
                                child: Image.asset(
                                  'images/Homepage_logo_farm.png',
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 5),
                                    child: Text(
                                      "FARM KIDS 1",
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Color(0xFF364AFC),
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                  Text(
                                    "Rose",
                                    style: TextStyle(
                                        fontSize: 13.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Divider(
                      height: 0,
                      thickness: 1.5,
                      indent: 10,
                      endIndent: 10,
                      color: Color.fromARGB(175, 213, 213, 213),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 5, bottom: 5, left: 5),
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width,
                        height: 75,
                        child: Row(
                          children: [
                            SizedBox(
                              height: 100,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(45),
                                child: Image.asset(
                                  'images/Homepage_logo_farm.png',
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 5),
                                    child: Text(
                                      "FARM KIDS 2",
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Color(0xFF364AFC),
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                  Text(
                                    "Mali",
                                    style: TextStyle(
                                        fontSize: 13.0,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Divider(
                      height: 0,
                      thickness: 1.5,
                      indent: 10,
                      endIndent: 10,
                      color: Color.fromARGB(175, 213, 213, 213),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
