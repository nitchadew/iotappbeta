import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AppStoreage extends StatefulWidget {
  Widget child;
  AppStoreage({Key? key, required this.child}) : super(key: key);

  @override
  State<AppStoreage> createState() => _AppStoreageState();
}

class _AppStoreageState extends State<AppStoreage> {
  User? user;

  void setUser(User newUser) {
    setState(() {
      user = newUser;
    });
  }

  User? getUser() {
    return user;
  }

  void clearUser() {
    setState(() {
      user = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DataInherited(
      appStoreageState: this,
      child: widget.child,
    );
  }
}

class DataInherited extends InheritedWidget {
  DataInherited({Key? key, required this.child, required this.appStoreageState})
      : super(key: key, child: child);

  final Widget child;
  final _AppStoreageState appStoreageState;

  static _AppStoreageState? of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<DataInherited>()!
        .appStoreageState;
  }

  @override
  bool updateShouldNotify(DataInherited oldWidget) {
    return true;
  }
}
