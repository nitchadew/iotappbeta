import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyTextStyle {
  TextStyle etcStyle(Color color, double fontSize, FontWeight fontWeight) {
    return GoogleFonts.ubuntu(
      fontWeight: fontWeight,
      color: color,
      fontSize: fontSize,
    );
  }

  TextStyle cardTitleStyle() {
    return const TextStyle(
      fontWeight: FontWeight.w700,
      color: const Color(0xFF4F80F3),
      fontSize: 17,
    );
  }

  TextStyle cardSubTitleStyle() {
    return const TextStyle(
      fontWeight: FontWeight.w300,
      color: Colors.black,
      fontSize: 13,
    );
  }

  TextStyle pageTitleStyle() {
    return const TextStyle(
        fontSize: 22.0, color: Colors.white, fontWeight: FontWeight.w600);
  }
}

class StyleInherited extends InheritedWidget {
  StyleInherited({Key? key, required this.child})
      : super(key: key, child: child);

  final Widget child;
  final MyTextStyle myTextStyle = MyTextStyle();

  static StyleInherited? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<StyleInherited>();
  }

  @override
  bool updateShouldNotify(StyleInherited oldWidget) {
    return true;
  }
}
