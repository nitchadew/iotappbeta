// ignore_for_file: prefer_const_constructors

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:iotapp/ContactUsPage.dart';
import 'package:iotapp/FavPage.dart';
import 'package:iotapp/GoogleAuthentication.dart';
import 'package:iotapp/HomePage.dart';
import 'package:iotapp/NotiPage.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:iotapp/inherited/DataInherited.dart';

class Launcher extends StatefulWidget {
  Launcher({Key? key}) : super(key: key);

  @override
  State<Launcher> createState() => _LauncherState();
}

class _LauncherState extends State<Launcher> {
  String pageInDrawer = "";
  var scaffoldKey = GlobalKey<ScaffoldState>();
  int _selectedIndex = 0;
  final List<BottomNavigationBarItem> _menuBar = <BottomNavigationBarItem>[
    const BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: 'Home',
    ),
    const BottomNavigationBarItem(
      icon: const Icon(Icons.star),
      label: 'Favorite',
    ),
    const BottomNavigationBarItem(
      icon: Icon(Icons.notifications),
      label: 'Notification',
    ),
  ];
  final List<Widget> _pageWidget = [];
  @override
  void initState() {
    _pageWidget.add(HomePage(
      openDrawer: openDrawer,
    ));
    _pageWidget.add(FavPage());
    _pageWidget.add(NotiPage());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      drawer: myDrawer(),
      body: bodyPage(),
      bottomNavigationBar: SizedBox(
        height: 100,
        child: BottomNavigationBar(
          items: _menuBar,
          currentIndex: _selectedIndex,
          selectedItemColor: Theme.of(context).primaryColor,
          unselectedItemColor: Colors.grey,
          onTap: _onItemTapped,
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      pageInDrawer = "";
      _selectedIndex = index;
    });
  }

  void openDrawer() {
    scaffoldKey.currentState!.openDrawer();
  }

  void openPageFromDrawer(String page) {
    setState(() {
      pageInDrawer = page;
    });
  }

  Widget bodyPage() {
    if (pageInDrawer == "contact_us") {
      return ContactUsPage(
        openPageFromDrawer: openPageFromDrawer,
      );
    } else {
      return _pageWidget.elementAt(_selectedIndex);
    }
  }

  Widget myDrawer() {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.75,
      child: Drawer(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 50, bottom: 10),
                child: Row(
                  children: [
                    DataInherited.of(context)!.getUser() != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(17.0),
                            child: Image.network(
                              DataInherited.of(context)!
                                  .getUser()!
                                  .photoURL
                                  .toString(),
                              height: 50.0,
                              width: 50.0,
                            ),
                          )
                        : Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(17),
                              color: Colors.grey.shade200,
                            ),
                            width: 50,
                            height: 50,
                            child: Icon(Icons.person)),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(bottom: 5),
                              child: FittedBox(
                                fit: BoxFit.scaleDown,
                                child:
                                    DataInherited.of(context)!.getUser() != null
                                        ? Text(
                                            DataInherited.of(context)!
                                                .getUser()!
                                                .displayName
                                                .toString(),
                                            style: TextStyle(
                                                fontSize: 17.0,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w700),
                                          )
                                        : Text(
                                            "BOB",
                                            style: TextStyle(
                                                fontSize: 17.0,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w700),
                                          ),
                              ),
                            ),
                            FittedBox(
                              fit: BoxFit.scaleDown,
                              child:
                                  DataInherited.of(context)!.getUser() != null
                                      ? Text(
                                          DataInherited.of(context)!
                                              .getUser()!
                                              .email
                                              .toString(),
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w300),
                                        )
                                      : Text(
                                          "bob@one.th",
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              color: Colors.black,
                                              fontWeight: FontWeight.w300),
                                        ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: [
                    ListTile(
                      title: const Text('Menu 1'),
                      onTap: () {},
                    ),
                    ListTile(
                      title: const Text('Menu 2'),
                      onTap: () {},
                    ),
                    ListTile(
                      title: const Text('Menu 2'),
                      onTap: () {},
                    ),
                    ListTile(
                      title: const Text('Menu 2'),
                      onTap: () {},
                    ),
                    Divider(
                      thickness: 1.5,
                      color: Colors.grey.shade200,
                    ),
                    ListTile(
                      title: Row(
                        children: [
                          Icon(
                            Icons.settings,
                            color: Colors.grey.shade400,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text("Setting"),
                          )
                        ],
                      ),
                      onTap: () {},
                    ),
                    ListTile(
                      title: Row(
                        children: [
                          Icon(
                            Icons.mode_edit,
                            color: Colors.grey.shade400,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text("Edit Profile"),
                          )
                        ],
                      ),
                      onTap: () {},
                    ),
                    ListTile(
                      title: Row(
                        children: [
                          Icon(
                            Icons.contact_support,
                            color: Colors.grey.shade400,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text("Contact Us"),
                          )
                        ],
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        openPageFromDrawer("contact_us");
                      },
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, bottom: 30),
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(35.0),
                    ),
                    side: BorderSide(width: 1, color: Color(0xff4F80F3)),
                  ),
                  onPressed: () async {
                    CoolAlert.show(
                      backgroundColor: Color(0xff4F80F3),
                      context: context,
                      type: CoolAlertType.loading,
                      text: "Logging out...",
                    );
                    if (await GoogleAuthentication.signOut(context: context)) {
                      Future.delayed(const Duration(milliseconds: 1000), () {
                        DataInherited.of(context)!.clearUser();
                        Navigator.of(context)
                            .popUntil((route) => route.isFirst);
                      });
                    } else {
                      print("cant logout");
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 22, bottom: 22, left: 10, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [Text('Sign Out'), Icon(Icons.logout)],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
