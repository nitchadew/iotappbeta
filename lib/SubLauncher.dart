// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:iotapp/Device.dart';
import 'package:iotapp/Menu.dart';

class SubLauncher extends StatefulWidget {
  String serviceName;
  SubLauncher({Key? key, required this.serviceName}) : super(key: key);

  @override
  State<SubLauncher> createState() => _SubLauncherState();
}

class _SubLauncherState extends State<SubLauncher> {
  int _selectedIndex = 0;
  final List<BottomNavigationBarItem> _menuBar = <BottomNavigationBarItem>[
    BottomNavigationBarItem(
      icon: Icon(Icons.menu),
      label: 'Menu',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.multitrack_audio),
      label: 'Device',
    ),
  ];
  List pageWidget = [];

  @override
  void initState() {
    pageWidget.add(Menu(
      serviceName: widget.serviceName,
    ));
    pageWidget.add(Device(
      serviceName: widget.serviceName,
    ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageWidget.elementAt(_selectedIndex),
      bottomNavigationBar: SizedBox(
        height: 100,
        child: BottomNavigationBar(
          items: _menuBar,
          currentIndex: _selectedIndex,
          selectedItemColor: Theme.of(context).primaryColor,
          unselectedItemColor: Colors.grey,
          onTap: _onItemTapped,
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
