// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:iotapp/inherited/StyleInherited.dart';

class Device extends StatefulWidget {
  String serviceName;
  Device({Key? key, required this.serviceName}) : super(key: key);

  @override
  State<Device> createState() => _DeviceState();
}

class _DeviceState extends State<Device> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('images/Favorite_bg.png'), fit: BoxFit.fill),
      ),
      child: Column(children: [
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.050,
        ),
        Stack(children: [
          Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child: ClipOval(
              child: Material(
                color: Colors.transparent, // Button color
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: SizedBox(
                      width: 56,
                      height: 56,
                      child: Icon(
                        Icons.arrow_back,
                        size: 27,
                        color: Colors.white,
                      )),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: widget.serviceName == "visie"
                ? SizedBox(
                    height: 56,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("VISIE HEALTH TRACKING",
                          style: StyleInherited.of(context)!
                              .myTextStyle
                              .pageTitleStyle()),
                    ),
                  )
                : SizedBox(
                    height: 56,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("FARM KIDS",
                          style: StyleInherited.of(context)!
                              .myTextStyle
                              .pageTitleStyle()),
                    ),
                  ),
          )
        ])
      ]),
    );
  }
}
