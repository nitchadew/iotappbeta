// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:iotapp/ScanQR.dart';
import 'package:iotapp/SubLauncher.dart';
import 'package:iotapp/inherited/DataInherited.dart';
import 'package:iotapp/inherited/StyleInherited.dart';

class ServiceData {
  ServiceData(
      {required this.serviceName,
      required this.title,
      required this.imgPath,
      required this.deviceNumber});
  String serviceName;
  String title;
  String imgPath;
  int deviceNumber;
}

class HomePage extends StatefulWidget {
  Function openDrawer;
  HomePage({Key? key, required this.openDrawer}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  late AnimationController animationController;
  String greetingWord = "";
  List serviceList = [];

  @override
  void initState() {
    serviceList.add(ServiceData(
        serviceName: "visie",
        title: "VISIE HEALTH\nTRACKING",
        imgPath: "images/Homepage_logo_vise.png",
        deviceNumber: 3));
    serviceList.add(ServiceData(
        serviceName: "farm_kids",
        title: "FARM KIDS",
        imgPath: "images/Homepage_logo_farm.png",
        deviceNumber: 2));
    getGreeting();
    animationController = AnimationController(
      vsync: this, // the SingleTickerProviderStateMixin
      duration: const Duration(milliseconds: 1100),
    );
    animationController.addListener(() {
      setState(() {});
    });
    animationController.forward();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var tween = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(
        parent: animationController, curve: Curves.fastOutSlowIn));
    return Material(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/Homepage_bg.png'), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.06,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      widget.openDrawer();
                    },
                    child: DataInherited.of(context)!.getUser() != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(17.0),
                            child: Image.network(
                              DataInherited.of(context)!
                                  .getUser()!
                                  .photoURL
                                  .toString(),
                              height: 50.0,
                              width: 50.0,
                            ),
                          )
                        : Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(17),
                              color: Colors.white60,
                            ),
                            width: 50,
                            height: 50,
                            child: Icon(Icons.person)),
                  ),
                  Image.asset(
                    'images/iot_logo_2.png',
                    fit: BoxFit.fitHeight,
                    height: 65,
                  ),
                  // add button

                  ClipOval(
                    child: Material(
                      color: Colors.transparent, // Button color
                      child: InkWell(
                        onTap: () {
                          Future.delayed(const Duration(milliseconds: 250), () {
                            Navigator.push(
                                context,
                                PageRouteBuilder(
                                    transitionsBuilder: (context, animation,
                                        secondaryAnimation, child) {
                                      const begin = Offset(1.0, 0.0);
                                      const end = Offset.zero;
                                      const curve = Curves.fastOutSlowIn;
                                      var tween = Tween(begin: begin, end: end)
                                          .chain(CurveTween(curve: curve));
                                      final offsetAnimation =
                                          animation.drive(tween);
                                      return SlideTransition(
                                        position: offsetAnimation,
                                        child: child,
                                      );
                                    },
                                    transitionDuration:
                                        Duration(milliseconds: 400),
                                    pageBuilder: (_, __, ___) =>
                                        ScanQR())).then((_) {
                              Future.delayed(const Duration(milliseconds: 150),
                                  () {
                                animationController.forward();
                              });
                            });
                          });
                        },
                        child: SizedBox(
                            width: 56,
                            height: 56,
                            child: Icon(
                              Icons.add,
                              size: 35,
                              color: Colors.white,
                            )),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Opacity(
              opacity: tween.value,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 45),
                child: Row(
                  children: [
                    DataInherited.of(context)!.getUser() != null
                        ? Expanded(
                            child: FittedBox(
                              alignment: Alignment.topLeft,
                              fit: BoxFit.scaleDown,
                              child: Text(
                                  greetingWord +
                                      "\n" +
                                      DataInherited.of(context)!
                                          .getUser()!
                                          .displayName
                                          .toString(),
                                  style: TextStyle(
                                      fontSize: 28,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w300)),
                            ),
                          )
                        : Text(greetingWord,
                            style: TextStyle(
                                fontSize: 28,
                                color: Colors.white,
                                fontWeight: FontWeight.w300)),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Opacity(
                opacity: tween.value,
                child: Transform.translate(
                  offset: Offset(0, -3 + (tween.value * 3)),
                  child: ListView.builder(
                    padding: EdgeInsets.only(top: 20),
                    itemCount: serviceList.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          animationController.reverse();
                          Navigator.push(
                              context,
                              PageRouteBuilder(
                                  transitionsBuilder: (context, animation,
                                      secondaryAnimation, child) {
                                    const begin = Offset(1.0, 0.0);
                                    const end = Offset.zero;
                                    const curve = Curves.fastOutSlowIn;
                                    var tween = Tween(begin: begin, end: end)
                                        .chain(CurveTween(curve: curve));
                                    final offsetAnimation =
                                        animation.drive(tween);
                                    return SlideTransition(
                                      position: offsetAnimation,
                                      child: child,
                                    );
                                  },
                                  transitionDuration:
                                      Duration(milliseconds: 400),
                                  pageBuilder: (_, __, ___) => SubLauncher(
                                        serviceName:
                                            serviceList[index].serviceName,
                                      ))).then((_) {
                            Future.delayed(const Duration(milliseconds: 150),
                                () {
                              animationController.forward();
                            });
                          });
                        },
                        child: Padding(
                          padding:
                              EdgeInsets.only(left: 40, right: 40, bottom: 20),
                          child: Container(
                            padding: EdgeInsets.all(30),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(35),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.2),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(0, 3),
                                ),
                              ],
                            ),
                            width: MediaQuery.of(context).size.width,
                            height: 150,
                            child: Row(
                              children: [
                                SizedBox(
                                  height: 90,
                                  child: Image.asset(
                                    serviceList[index].imgPath,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 5),
                                        child: Text(serviceList[index].title,
                                            style: StyleInherited.of(context)!
                                                .myTextStyle
                                                .cardTitleStyle()),
                                      ),
                                      Text(
                                        serviceList[index]
                                                .deviceNumber
                                                .toString() +
                                            " Devices",
                                        style: StyleInherited.of(context)!
                                            .myTextStyle
                                            .cardSubTitleStyle(),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void getGreeting() {
    final now = DateTime.now();

    if (now.hour > 5 && now.hour < 13) {
      setState(() {
        greetingWord = "GOOD MORNING";
      });
    } else if (now.hour >= 13 && now.hour < 18) {
      setState(() {
        greetingWord = "GOOD AFTERNOON";
      });
    } else if (now.hour >= 18) {
      setState(() {
        greetingWord = "GOOD EVENING";
      });
    }
  }
}
