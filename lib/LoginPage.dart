// ignore_for_file: prefer_const_constructors

import 'package:cool_alert/cool_alert.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:iotapp/GoogleAuthentication.dart';
import 'package:iotapp/inherited/DataInherited.dart';
import 'package:iotapp/Launcher.dart';

import 'MyWebView.dart';

class Loginpage extends StatefulWidget {
  Loginpage({Key? key}) : super(key: key);

  @override
  State<Loginpage> createState() => _LoginpageState();
}

class _LoginpageState extends State<Loginpage> with TickerProviderStateMixin {
  String signInUrl =
      "https://one.th/oauth/authorize?client_id=433&response_type=code&scope=";
  String signUpUrl = "https://one.th/register";

  late AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
      vsync: this, // the SingleTickerProviderStateMixin
      duration: const Duration(milliseconds: 1300),
    );
    animationController.addListener(() {
      setState(() {});
    });
    animationController.forward();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var tween = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(
        parent: animationController, curve: Curves.fastOutSlowIn));
    return Material(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/Login_bg_1.png'), fit: BoxFit.fill),
        ),
        child: Opacity(
          opacity: tween.value,
          child: Transform.translate(
            offset: Offset(0, -22 + (tween.value * 22)),
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.15,
                ),
                Image.asset(
                  'images/iot_logo_3.png',
                  fit: BoxFit.fitWidth,
                  width: 280,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.13,
                ),
                Expanded(
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: [
                      Positioned(
                        child: SizedBox(
                          child: Image.asset(
                            'images/Login_decorations.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      // sign with google
                      Positioned(
                        bottom: 60.0,
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 15),
                              child: GestureDetector(
                                onTap: () async {
                                  animationController.reverse();
                                  User? user = await GoogleAuthentication
                                      .signInWithGoogle(context: context);
                                  if (user != null) {
                                    CoolAlert.show(
                                      backgroundColor: Color(0xff4F80F3),
                                      context: context,
                                      type: CoolAlertType.loading,
                                      text: "Logging In...",
                                    );
                                    DataInherited.of(context)!.setUser(user);
                                    Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                            transitionDuration:
                                                Duration(milliseconds: 0),
                                            pageBuilder: (_, __, ___) =>
                                                Launcher())).then((_) {
                                      Future.delayed(
                                          const Duration(milliseconds: 150),
                                          () {
                                        animationController.forward();
                                      });
                                    });
                                  } else {
                                    animationController.forward();
                                  }
                                },
                                child: Image.asset(
                                  'images/Login_button_sign_in_google_1.png',
                                  fit: BoxFit.fitWidth,
                                  width: 350,
                                ),
                              ),
                            ),
                            // sign with one
                            Padding(
                              padding: const EdgeInsets.only(bottom: 15),
                              child: GestureDetector(
                                onTap: () {
                                  animationController.reverse();
                                  Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                          transitionDuration:
                                              Duration(milliseconds: 350),
                                          pageBuilder: (_, __, ___) =>
                                              Launcher())).then((_) {
                                    Future.delayed(
                                        const Duration(milliseconds: 150), () {
                                      animationController.forward();
                                    });
                                  });
                                },
                                child: Image.asset(
                                  'images/Login_button_sign_in_1.png',
                                  fit: BoxFit.fitWidth,
                                  width: 350,
                                ),
                              ),
                            ),
                            Row(
                              children: [
                                Text(
                                  "Don’t have an account? ",
                                  style: TextStyle(fontSize: 18),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                            transitionDuration:
                                                Duration(milliseconds: 350),
                                            pageBuilder: (_, __, ___) =>
                                                MyWebView(
                                                  appBarText: "Sing Up One ID",
                                                  url: signUpUrl,
                                                  isTokenGranted: () {},
                                                )));
                                  },
                                  child: Text(
                                    "Sign up",
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xFF4F80F3),
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
