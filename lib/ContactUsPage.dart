// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:iotapp/inherited/StyleInherited.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactUsPage extends StatefulWidget {
  Function openPageFromDrawer;
  ContactUsPage({Key? key, required this.openPageFromDrawer}) : super(key: key);

  @override
  State<ContactUsPage> createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage>
    with TickerProviderStateMixin {
  late AnimationController animationController;
  String greetingWord = "";
  List serviceList = [];

  @override
  void initState() {
    animationController = AnimationController(
      vsync: this, // the SingleTickerProviderStateMixin
      duration: const Duration(milliseconds: 1100),
    );
    animationController.addListener(() {
      setState(() {});
    });
    animationController.forward();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double cardHeight = 135;
    var tween = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(
        parent: animationController, curve: Curves.fastOutSlowIn));
    return Column(
      children: [
        Stack(alignment: Alignment.center, children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(65.0),
                  bottomRight: Radius.circular(65.0)),
              color: Color(0xff5960F9),
              boxShadow: [
                BoxShadow(
                  color: Color(0xff87F0FF),
                  spreadRadius: 1,
                  blurRadius: 0,
                  offset: Offset(1, 2),
                ),
              ],
            ),
            height: MediaQuery.of(context).size.height * 0.116,
          ),
          Positioned(
            left: 15,
            top: MediaQuery.of(context).size.height * 0.045,
            child: ClipOval(
              child: Material(
                color: Colors.transparent, // Button color
                child: InkWell(
                  onTap: () {
                    widget.openPageFromDrawer("");
                  },
                  child: SizedBox(
                      width: 56,
                      height: 56,
                      child: Icon(
                        Icons.arrow_back,
                        size: 27,
                        color: Colors.white,
                      )),
                ),
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.045,
            child: SizedBox(
              height: 56,
              child: Align(
                alignment: Alignment.center,
                child: Text("CONTACT US",
                    style: StyleInherited.of(context)!
                        .myTextStyle
                        .pageTitleStyle()),
              ),
            ),
          )
        ]),
        Expanded(
          child: SingleChildScrollView(
            child: Opacity(
              opacity: tween.value,
              child: Transform.translate(
                offset: Offset(0, -5 + (tween.value * 5)),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 30,
                        ),
                        child: Text("Address",
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey.shade700)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 20,
                          bottom: 20,
                        ),
                        child: Text(
                            "บริษัท อินเทอร์เน็ตประเทศไทย จำกัด (มหาชน) \n 1768 อาคารไทยซัมมิท ทาวเวอร์ ชั้น 10-12 และชั้น IT \n ถ.เพชรบุรีตัดใหม่ แขวงบางกะปิ \n เขตห้วยขวาง กรุงเทพมหานคร 10310",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.grey.shade600, fontSize: 17)),
                      ),
                      //call
                      Padding(
                        padding: EdgeInsets.only(bottom: 20),
                        child: GestureDetector(
                          onTap: () {
                            callPhone();
                          },
                          child: Container(
                            padding: EdgeInsets.all(30),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(35),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.2),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(0, 3),
                                ),
                              ],
                            ),
                            width: MediaQuery.of(context).size.width,
                            height: cardHeight,
                            child: Row(
                              children: [
                                CircleAvatar(
                                  radius: 42,
                                  backgroundColor: Color(0xFF779FFF),
                                  child: const Icon(Icons.phone,
                                      size: 38, color: Colors.white),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 5),
                                        child: Text(
                                          "085-383-9966",
                                          style: StyleInherited.of(context)!
                                              .myTextStyle
                                              .cardTitleStyle(),
                                        ),
                                      ),
                                      // Text(
                                      //   "Tel2",
                                      //   style: StyleInherited.of(context)!
                                      //       .myTextStyle
                                      //       .cardTitleStyle(),
                                      // ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      //mail
                      Padding(
                        padding: EdgeInsets.only(bottom: 20),
                        child: GestureDetector(
                          onTap: () {
                            sendMail();
                          },
                          child: Container(
                            padding: EdgeInsets.all(30),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(35),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.2),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(0, 3),
                                ),
                              ],
                            ),
                            width: MediaQuery.of(context).size.width,
                            height: cardHeight,
                            child: Row(
                              children: [
                                CircleAvatar(
                                  radius: 42,
                                  backgroundColor: Color(0xFFA889FF),
                                  child: const Icon(Icons.mail,
                                      size: 37, color: Colors.white),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 5),
                                        child: Text(
                                          "info@inet.co.th",
                                          style: StyleInherited.of(context)!
                                              .myTextStyle
                                              .cardTitleStyle(),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 20),
                        child: Container(
                          padding: EdgeInsets.all(30),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(35),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 2,
                                blurRadius: 7,
                                offset: Offset(0, 3),
                              ),
                            ],
                          ),
                          width: MediaQuery.of(context).size.width,
                          height: cardHeight,
                          child: Row(
                            children: [
                              CircleAvatar(
                                radius: 42,
                                backgroundColor: Color(0xFF76C5FF),
                                child: const Icon(Icons.wechat,
                                    size: 40, color: Colors.white),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 5),
                                      child: Text(
                                        "OneChat",
                                        style: StyleInherited.of(context)!
                                            .myTextStyle
                                            .cardTitleStyle(),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  void sendMail() async {
    final Uri _url =
        Uri.parse('mailto:info@inet.co.th?subject=ติดต่อ สอบถาม&body=');
    if (!await launchUrl(_url)) throw 'Could not launch $_url';
  }

  void callPhone() async {
    final Uri _url = Uri.parse('tel:+66-853-839-966');
    if (!await launchUrl(_url)) throw 'Could not launch $_url';
  }
}
