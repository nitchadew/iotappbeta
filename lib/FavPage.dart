// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:iotapp/inherited/StyleInherited.dart';

class FavDeta {
  FavDeta(
      {required this.title, required this.description, required this.imgPath});

  String title;
  String description;
  String imgPath;
}

class FavPage extends StatefulWidget {
  FavPage({Key? key}) : super(key: key);

  @override
  State<FavPage> createState() => _FavPageState();
}

class _FavPageState extends State<FavPage> with TickerProviderStateMixin {
  late AnimationController animationScaleController;
  List favList = [];

  @override
  void initState() {
    animationScaleController = AnimationController(
      vsync: this, // the SingleTickerProviderStateMixin
      duration: const Duration(milliseconds: 1000),
    );
    animationScaleController.addListener(() {
      setState(() {});
    });
    favList.add(FavDeta(
        title: "VISIE 1",
        description: "OAI floor 4",
        imgPath: 'images/Homepage_logo_vise.png'));
    favList.add(FavDeta(
        title: "VISIE 2",
        description: "OAI floor 5",
        imgPath: 'images/Homepage_logo_vise.png'));
    favList.add(FavDeta(
        title: "Farm Kids",
        description: "My Sunflovers",
        imgPath: 'images/Homepage_logo_farm.png'));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var tweenScale = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(
        parent: animationScaleController, curve: Curves.bounceInOut));
    return Material(
      child: Column(
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(65.0),
                      bottomRight: Radius.circular(65.0)),
                  color: Color(0xff5960F9),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff87F0FF),
                      spreadRadius: 1,
                      blurRadius: 0,
                      offset: Offset(1, 2),
                    ),
                  ],
                ),
                height: MediaQuery.of(context).size.height * 0.116,
              ),
              Positioned(
                top: MediaQuery.of(context).size.height * 0.045,
                child: SizedBox(
                  height: 56,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("FAVORITE",
                        style: StyleInherited.of(context)!
                            .myTextStyle
                            .pageTitleStyle()),
                  ),
                ),
              )
            ],
          ),
          Expanded(
            child: ListView.builder(
                itemCount: favList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.only(left: 40, right: 40, bottom: 20),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(35),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.2),
                            spreadRadius: 2,
                            blurRadius: 7,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      width: 500,
                      height: 140,
                      child: Slidable(
                        key: UniqueKey(),
                        endActionPane: ActionPane(
                          extentRatio: 0.35,
                          motion: ScrollMotion(),
                          children: [
                            SlidableAction(
                              onPressed: (context) {
                                var name = favList[index].title;
                                setState(() {
                                  favList.remove(favList[index]);
                                });
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content:
                                            Text(name + " has been removed.")));
                              },
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(30),
                                  bottomRight: Radius.circular(30)),
                              backgroundColor: Colors.red,
                              foregroundColor: Colors.white,
                              icon: Icons.delete,
                              label: 'Delete',
                            ),
                          ],
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(30),
                          child: Row(
                            children: [
                              SizedBox(
                                height: 90,
                                child: Image.asset(
                                  favList[index].imgPath,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 5),
                                      child: Text(
                                        favList[index].title,
                                        style: StyleInherited.of(context)!
                                            .myTextStyle
                                            .cardTitleStyle(),
                                      ),
                                    ),
                                    Text(
                                      favList[index].description,
                                      style: StyleInherited.of(context)!
                                          .myTextStyle
                                          .cardSubTitleStyle(),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
